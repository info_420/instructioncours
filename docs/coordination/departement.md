# Les tâches du coordonnateur départmental

## Rôles du coordonnateur


https://cegepdrummond.omnivox.ca/intr/webpart.gestion.consultation/Departement_comite_programme_et_coordination.pdf?IdWebPart=2EFCC2E6-9C06-4E40-89EA-79C62C662668&idDoc=ZDk1YTcxY2QtNDQ5Zi00ZDMwLTljODMtYmIzM2ZmOTdkMDI3&from=accueil%22


https://cegepdrummond.omnivox.ca/intr/webpart.gestion.consultation/responsabilites_programme_vs_departement.pdf?IdWebPart=aac1cd6d-b6cc-4408-a6a4-41778060dffe&idDoc=ZjBlYmViYjYtMDVjNS00MGMyLTkzZWQtMjM1ZTExZDQyZTZl&t=1715623278


https://cegepdrummond.omnivox.ca/intr/webpart.gestion.consultation/cadres_de_reference.pdf?IdWebPart=aac1cd6d-b6cc-4408-a6a4-41778060dffe&idDoc=MWU5ZDc3MWEtMDY5OS00M2EzLTg1MWItYmMzOWM5ZGZiNjJh&t=1715623278

## Vignette de stationnement temporaire pour des visiteurs

Acheminer une demande à la direction (Maryse Boucher) pour obtenir une vignette temporaire lorsqu'un invité doit venir au cégep (client de web 1 et 2, présentation d'entreprise, etc.)

## Activités annuelles 

* Voir le [document de Francois ](https://info420.cegepdrummond.ca/nuage/index.php/apps/files/?dir=/Departement/D%C3%A9partement/Documents%20officiels/Guide%20des%20coordonnateurs&fileid=416784)

* Envoyer le plan de perfectionnement (date ?) [Plan de perfectionnement](/Plan%20de%20perfectionnement_2024-2025.zip)