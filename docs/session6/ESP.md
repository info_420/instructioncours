# Épreuve Synthèse de Programme


## Historique des enseignants

2024 Benoit Desrosiers, Benoit Tremblay, Jonathan Gareau, Fréderic Montambeault

## Notes de cours

L'ESP est associé aux stages. 

Il n'y pas de notes de cours. Mais des documents expliquant les tâches à faire est envoyé à l'avance (voir ci-bas)

Suite à une discussion de groupe, il a été décidé que l'utilisation de l'IA serait permise. Par contre, tout code généré devra être très bien maitrisé par l'étudiant. Il doit s'attendre à devoir expliquer en détail le code généré. 



### 2024

mettre les documents Enonce et contexte

#### À améliorer

### 2023

## Planification du cours durant la session

L'ESP est divisée en 3 composants:

### WEB

Pour la partie web, les étudiants ont fait l'inspection de code d'un autre projet dans le cours Projet Web 1 et 2 (équipe 1 fait l'inspection pour l'équipe 2, qui le fait pour 3 .... qui le fait pour 1).

À la semaine 5 du stage, un rappel est envoyé aux étudiants pour leur indiquer en quoi consistera l'ESP Web. 

Si un étudiant qui fait son stage n'est pas dans le cours Projet web 1 et 2, il est important de lui associer un projet web à la semaine 5 des stages (semaine 11-12 de la session) (ou avant) afin qu'il ait la chance de connaitre le code. 

Habituellement, l'enseignant de Projet web 2 aide à déterminer les questions qui seront posées lors de l'ESP. Il est aussi recommandé qu'un des enseignants de l'ESP passe quelques temps dans ce cours afin de se familiariser avec les projets (si le prof de projet web 2 ne fait pas parti de l'équipe de l'ESP)

(mettre un exemple du document )

### Native

Pour la partie native, les étudiants recoivent une description des langages/bibliothèques/framework qu'ils auront à utiliser pour l'ESP. 

Ils ont le choix entre 2 projets. 

(mettre un exemple du document)

### Réseau

Pour la partie réseau, les étudiants recoivent un diagramme du genre de réseau qu'ils auront à réaliser.

Lors de l'ESP, une description plus précise leur est fournie. 

(mettre un exemple)

## Aménagement d'horaire

L'ESP se tient durant la semaine d'examen. 

Il est important de réserver le local (habituellement le 1204 pour la partie réseau) lors de la réception de l'avis de réservation pour les examens de fin de session. 

Réserver le local sur 3 jours: la première journée est pour s'assurer que le local est en bonne condition pour la partie réseau. Les 2 autres jours sont pour l'ESP qui commence à 8h et se termine à la fermeture du cégep pour la première journée, et recommence à 8h le lendemain pour se terminer vers 17h. 

Les étudiants ont le droit de travailler sur leur projets durant la nuit. Une vérification est fait au matin afin de s'assurer que le code ajouté est compris. 



## Site web/livre en référence


