---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "420 Instructions pour les cours"
  text: "Choses à connaitre pour l'installation des logiciels ou les choses à faire à telle date pour chacun des cours. "
  tagline: My great project tagline
 

features:
  - title: Coordination départementale
    link: /coordination/departement
    linkText: y allez 
  - title: Cours
    link: /cheminement
    linkText: y allez  
  - title: Documents officiels
    link: /documents
    linkText: y allez  
  - title: Qui fait quoi?
    details: Liste des rôles de la direction
    link: https://cegepdrummond.omnivox.ca/intr/webpart.gestion.consultation/qui_fait_quoi_2024-08-26.pdf?IdWebPart=9ce11350-ab65-4fab-be58-202d2d4107fb&idDoc=ZjEwMmYzYWQtYzQxNy00ZTA4LTgzNzUtYjQ4NWEwNWUzY2Ux&t=1724681228
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

