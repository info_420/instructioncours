import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  outDir: "../public",
  base:"/instructioncours",
  title: "420_Instruction_cours",
  description: "Choses à connaitre pour l'installation des logiciels ou les choses à faire à telle date pour chacun des cours. ",
  themeConfig: {
    docFooter: {
      prev: "Page précédente",
      next: "Page suivante"
    },
    outline:{
      label: "Sur cette page"
    },
    search: {
      provider: 'local'
    },
    nav: [
      { text: 'Accueil', link: '/' },
      { text: 'Cheminement', link: '/cheminement' },
    ],

    sidebar: [
      { 
        text: 'Cheminement',
        collapsed: true,
        items: [
          { text: 'Cheminement', link: '/cheminement' },
        ],
      },
      { 
        text: 'Coordination',
        collapsed: true,
        items: [
          { text: 'Départementale', link: '/coordination/departement' },
          { text: 'Programme', link: '/coordination/programme' },
          { text: 'Centre d\'aide (CATI)', link: '/coordination/cati' },


        ],
      },
      { 
        text: 'Documents',
        collapsed: true,
        items: [
          { text: 'Liens', link: '/documents' },

        ],
      },
      { 
        text: 'Projets',
        collapsed: true,
        items: [
          { text: 'International PR02', link: '/projets/international' },

        ],
      },
      {
        text: 'Session 1',
        collapsed: true,
        items: [
          { text: '1R1 Init à l\'ordi', link: '/session1/1R1' },
          { text: '1N1 Init à la programmation', link: '/session1/1N1' },
          { text: '1W1 Web 1', link: '/session1/1W1' },


        ]
      },
      {
        text: 'Session 2',
        collapsed: true,
        items: [
          { text: '2N1 POO 1', link: '/session2/2N1' },
          { text: '2N2 Native 1', link: '/session2/2N2' },
          { text: '2N3 Outils', link: '/session2/2N3' },
          { text: '2W1 Web 2', link: '/session2/2W1' },

        ]
      },
      {
        text: 'Session 3',
        collapsed: true,
        items: [
          { text: '3R1 Réseau 1', link: '/session3/3R1' },
          { text: '3N1 POO2', link: '/session3/3N1' },
          { text: '3N2 Native 2', link: '/session3/3N2' },
          { text: '3N3 BD', link: '/session3/3N3' },
          { text: '3W1 Web 3', link: '/session3/3W1' },
        ]
      },
      {
        text: 'Session 4',
        collapsed: true,
        items: [
          { text: '4R1 Syst expl', link: '/session4/4R1' },
          { text: '4P1 Analyse', link: '/session4/4P1' },
          { text: '4W1 Web 4', link: '/session4/4W1' },
          { text: '4N1 Native 3', link: '/session4/4N1' },
        ]
      },
      {
        text: 'Session 5',
        collapsed: true,
        items: [
          { text: '5R1 Réseau 2', link: '/session5/5R1' },
          { text: '5P2 Préparation au stage', link: '/session5/5P2' },
          { text: '5P3 Nouvelle techno', link: '/session5/5P3' },
          { text: '5W2 Projet web 1', link: '/session5/5W2' },
        ]
      },
      {
        text: 'Session 6',
        collapsed: true,
        items: [
          { text: '6R1 Soutien technique', link: '/session6/6R1' },
          { text: '6S1 Stages', link: '/session6/6S1' },
          { text: '6N1 Native 4', link: '/session6/6N1' },
          { text: '6W1 Projet web 2', link: '/session6/6W1' },
          { text: 'ESP', link: '/session6/ESP' },

        ]
      },
      
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  }
})
