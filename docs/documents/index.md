# Liens vers les documents officiels







[PDEA](https://cegepdrummond.omnivox.ca/intr/webpart.gestion.consultation/Techniques_de_linformatique_ANNEXES_2025-01-09.pdf?IdWebPart=A4DA4991-E872-446D-B422-2D76CBD47DB0&idDoc=ZmYzOWMwNzAtYjYwMi00ZjkwLWJmNzUtMTRkNGM3MjBlMWIz&from=accueil%22)

[PDEA source](https://info420.cegepdrummond.ca/nuage/index.php/apps/files/?dir=/Departement/D%C3%A9partement/Documents%20officiels/PDEA&fileid=3353)

[PIEA](https://www.cegepdrummond.ca/wp-content/uploads/2024/07/POL_01_PIEA_2017_11_28.pdf)

[Plans cadre](https://cegepdrummond.sharepoint.com/sites/refonteduprogramme/Documents%20partages/Forms/AllItems.aspx?id=%2Fsites%2Frefonteduprogramme%2FDocuments%20partages%2F2%2E%20Plans%2Dcadre&viewid=748d471a%2D0b33%2D452c%2Dab35%2D9958b48ea788)

[Plans de cours ](https://info420.cegepdrummond.ca/nuage/index.php/apps/files/?dir=/Departement/D%C3%A9partement&fileid=982) /année/Plan de cours/session 

[Refonte du programme](https://cegepdrummond.sharepoint.com/sites/refonteduprogramme/Documents%20partages/Forms/AllItems.aspx?viewid=748d471a%2D0b33%2D452c%2Dab35%2D9958b48ea788)